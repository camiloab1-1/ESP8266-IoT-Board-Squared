View this project on [CADLAB.io](https://cadlab.io/project/23627). 

# ESP8266-IoT-Board-Squared

## Hola Mundo!!

###Este es un circuito impreso con el módulo ESP-12S que contiene el MCU ESP8266, con sus correspondientes breakouts para pines extra, un sensor de humedad/temperatura DHT11 y un sensor de humedad/temperatura/presion/VOC industrial marca Bosch BME680 como agregado, que funciona por interfaz HS-SPI. También contiene archivos STEP sin pistas, y otro con pistas y vias.
